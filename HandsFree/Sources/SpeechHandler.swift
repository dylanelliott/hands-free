//
//  SpeechHandler.swift
//  HandsFree
//
//  Created by Dylan Elliott on 29/6/18.
//  Copyright © 2018 Dylan Elliott. All rights reserved.
//

import Foundation
import Speech

protocol SpeechHandlerDelegate {
    func recordingDidBegin()
    func recordingDidEnd()
    func speechRecognizerDidProduceResult(result: SFSpeechRecognitionResult)
}

class SpeechHandler {
    private lazy var audioEngine: AVAudioEngine = {
        let audioEngine = AVAudioEngine()
        let node = audioEngine.inputNode
        let recordingFormat = node.outputFormat(forBus: 0)
        
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { [unowned self] (buffer, _) in
            self.request.append(buffer)
        }
        
        return audioEngine
    }()
    
    private let speechRecognizer = SFSpeechRecognizer()
    private let request = SFSpeechAudioBufferRecognitionRequest()
    private var recognitionTask: SFSpeechRecognitionTask?
    
    var delegate: SpeechHandlerDelegate?
    
    var isRecording: Bool {
        return audioEngine.isRunning
    }
    
    func requestSpeechAuthorisation(completion: @escaping (Bool) -> Void) {
        SFSpeechRecognizer.requestAuthorization { status in
            completion(status == .authorized)
        }
    }
    
    func startRecording() throws {
        
        audioEngine.prepare()
        try audioEngine.start()
        
        recognitionTask = speechRecognizer?.recognitionTask(with: request) { [weak self] (result, _) in
            
            guard let result = result else { return }
            
            self?.delegate?.speechRecognizerDidProduceResult(result: result)
            
            if result.isFinal {
                self?.stopRecording()
            }
        }
        
        delegate?.recordingDidBegin()
    }
    
    func stopRecording() {
        audioEngine.stop()
        request.endAudio()
        recognitionTask?.cancel()
        
        delegate?.recordingDidEnd()
    }
}
