//
//  ViewController.swift
//  HandsFree
//
//  Created by Dylan Elliott on 29/6/18.
//  Copyright © 2018 Dylan Elliott. All rights reserved.
//

import UIKit
import Speech
import Intents

class ViewController: UIViewController, SpeechHandlerDelegate, ShoppingIntentsDelegate {
    
    @IBOutlet var label: UILabel!
    @IBOutlet var recordingIndicator: UIView!

    lazy var speechHandler: SpeechHandler = {
        let speechHandler = SpeechHandler()
        speechHandler.delegate = self
        return speechHandler
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let recogniser = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(recogniser)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let intent = ShowListIntent()
        let interaction = INInteraction(intent: intent, response: nil)
        interaction.donate(completion: { error in
            if let error = error {
                self.alert(title: "Error", message: "Could not donate intent: \(error.localizedDescription)")
            }
        })
        
        speechHandler.requestSpeechAuthorisation { success in
            
            let intent = StartShoppingIntent()
            let interaction = INInteraction(intent: intent, response: nil)
            interaction.donate(completion: { error in
                if let error = error {
                    self.alert(title: "Error", message: "Could not donate intent: \(error.localizedDescription)")
                }
            })
            
            if success {
                self.tryStartRecording()
            }
        }
    }
    
    @objc func viewTapped() {
        if !speechHandler.isRecording {
            tryStartRecording()
        }
    }
    
    func updateRecordingIndicator(recording: Bool) {
        DispatchQueue.main.async {
            self.recordingIndicator.backgroundColor = recording ? .red : .lightGray
        }
    }
    
    func tryStartRecording() {
        do {
            try self.speechHandler.startRecording()
        } catch let error {
            alert(title: "Error", message: "There was a problem starting recording: \(error.localizedDescription)")
        }
    }
    
    // MARK: - Conformance
    
    // MARK: SpeechHandlerDelegate
    
    func recordingDidBegin() {
        updateRecordingIndicator(recording: true)
    }
    
    func recordingDidEnd() {
        updateRecordingIndicator(recording: false)
    }
    
    func speechRecognizerDidProduceResult(result: SFSpeechRecognitionResult) {
        DispatchQueue.main.async {
            let transcription = result.bestTranscription
            self.label.text = transcription.formattedString
        }
    }
    
    // ShoppingEventsDelegate
    
    func startShoppingIntentReceived() {
        if !speechHandler.isRecording {
            tryStartRecording()
        }
        
        label.text = "Ready to Shop"
    }
    
    func showListIntentReceived() {
        if speechHandler.isRecording {
            speechHandler.stopRecording()
        }
        
        label.text = "Here's where your groceries would be..."
    }
}

