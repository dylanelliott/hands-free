//
//  UIViewController+Extensions.swift
//  HandsFree
//
//  Created by Dylan Elliott on 29/6/18.
//  Copyright © 2018 Dylan Elliott. All rights reserved.
//

import UIKit

extension UIViewController {
    func alert(title: String?, message: String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
}
