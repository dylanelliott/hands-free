//
//  AppDelegate.swift
//  HandsFree
//
//  Created by Dylan Elliott on 29/6/18.
//  Copyright © 2018 Dylan Elliott. All rights reserved.
//

import UIKit

protocol ShoppingIntentsDelegate {
    func startShoppingIntentReceived()
    func showListIntentReceived()
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var recordingViewController: ViewController? {
        guard let window = window, let viewController = window.rootViewController as? ViewController else { return nil }
        
        return viewController
    }


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    func application(_ application: UIApplication,
                     continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        guard let intent = userActivity.interaction?.intent else { return false }
        
        if intent is StartShoppingIntent {
            recordingViewController?.startShoppingIntentReceived()
            return true
        } else if intent is ShowListIntent {
            recordingViewController?.showListIntentReceived()
            return true
        }
        
        return false
    }


}

